<?php

namespace riskit\testPackage\Facades;

use Illuminate\Support\Facades\Facade;

class testPackage extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'testpackage';
    }

    public function test() {
    	dd("More Success!!!");
    }
}
