<?php

// Available placeholders: riskit, testPackage, riskit, testpackage
return [
    'src/MyPackage.php' => 'src/testPackage.php',
    'config/mypackage.php' => 'config/testpackage.php',
    'src/Facades/MyPackage.php' => 'src/Facades/testPackage.php',
    'src/MyPackageServiceProvider.php' => 'src/testPackageServiceProvider.php',
];